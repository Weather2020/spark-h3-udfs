package com.weather2020.spark.h3.udf;

import com.uber.h3core.H3Core;
import com.uber.h3core.util.GeoCoord;
import com.uber.h3core.LengthUnit;

import org.apache.spark.sql.api.java.UDF2;

import java.io.IOException;


public class H3PointDistance implements UDF2<Long, Long, Double> {
    static H3Core h3;

    static {
        try {
            h3 = H3Core.newInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Double call(Long a, Long b) throws Exception {
        GeoCoord aCoordinates = h3.h3ToGeo(a);
        GeoCoord bCoordinates = h3.h3ToGeo(b);
        return h3.pointDist(aCoordinates, bCoordinates, LengthUnit.km);
    }
}
