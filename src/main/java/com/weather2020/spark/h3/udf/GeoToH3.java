package com.weather2020.spark.h3.udf;

import com.uber.h3core.H3Core;
import org.apache.spark.sql.api.java.UDF3;

import java.io.IOException;


public class GeoToH3 implements UDF3<Float, Float, Integer, Long> {
    static H3Core h3;

    static {
        try {
            h3 = H3Core.newInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Long call(Float latitude, Float longitude, Integer resolution) throws Exception {
        return h3.geoToH3(latitude, longitude, resolution);
    }
}
