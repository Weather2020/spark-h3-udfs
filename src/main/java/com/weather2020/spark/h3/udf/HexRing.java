package com.weather2020.spark.h3.udf;

import com.uber.h3core.H3Core;
import org.apache.spark.sql.api.java.UDF2;

import java.io.IOException;
import java.util.List;


public class HexRing implements UDF2<Long, Integer, List<Long>> {
    static H3Core h3;

    static {
        try {
            h3 = H3Core.newInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Long> call(Long h, Integer res) throws Exception {
        return h3.hexRing(h, res);
    }
}
