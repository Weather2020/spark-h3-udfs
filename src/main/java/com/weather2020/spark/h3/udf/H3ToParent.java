package com.weather2020.spark.h3.udf;

import com.uber.h3core.H3Core;
import org.apache.spark.sql.api.java.UDF2;

import java.io.IOException;


public class H3ToParent implements UDF2<Long, Integer, Long> {
    static H3Core h3;

    static {
        try {
            h3 = H3Core.newInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Long call(Long h, Integer res) throws Exception {
        return h3.h3ToParent(h, res);
    }
}
