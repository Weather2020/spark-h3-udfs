package com.weather2020.spark.h3.udf;

import com.uber.h3core.H3Core;
import com.uber.h3core.util.GeoCoord;

import org.apache.spark.sql.api.java.UDF1;

import java.io.IOException;


public class H3ToGeo implements UDF1<Long, GeoCoord> {
    static H3Core h3;

    static {
        try {
            h3 = H3Core.newInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public GeoCoord call(Long h) throws Exception {
        return h3.h3ToGeo(h);
    }
}
